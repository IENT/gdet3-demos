{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Copyright 2020 Institut für Nachrichtentechnik, RWTH Aachen University\n",
    "%matplotlib widget\n",
    "from ipywidgets import interact, interactive, fixed, HBox, VBox\n",
    "import ipywidgets as widgets\n",
    "from IPython.display import clear_output, display, HTML\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "import rwth_nb.plots.mpl_decorations as rwth_plots\n",
    "import rwth_nb.misc.transforms as rwth_transforms\n",
    "from rwth_nb.misc.signals import *\n",
    "\n",
    "import warnings\n",
    "warnings.simplefilter(\"ignore\", UserWarning)  # TODO: strange masked array to nan warning\n",
    "\n",
    "signals_t = {'cos-Funktion':    lambda t: np.cos(2 * np.pi * t),\n",
    "             'sin-Funktion':    lambda t: np.sin(2 * np.pi * t),\n",
    "             'si-Funktion':     lambda t: si(2 * np.pi * t),\n",
    "             'Rechteckimpuls':  lambda t: rect(t / 1.05),\n",
    "             'Dreieckimpuls':   tri}\n",
    "\n",
    "signals_f = {'cos-Funktion':   lambda f, F: np.isin(f/F, f[find_ind_least_diff(f/F, [-1, 1])]/F) * 0.5,\n",
    "             'sin-Funktion':   lambda f, F: np.isin(f/F, f[find_ind_least_diff(f/F, [1])]/F) / 2j - np.isin(f/F, f[find_ind_least_diff(f/F, [-1])]/F) / 2j,\n",
    "             'si-Funktion':    lambda f, F: 1/(2*np.abs(F))*rect(f / (2*F)),\n",
    "             'Rechteckimpuls': lambda f, F: 1/np.abs(F)*si(np.pi * f / F),\n",
    "             'Dreieckimpuls':  lambda f, F: 1/np.abs(F)*si(np.pi * f/F) ** 2}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div>\n",
    "    <img src=\"figures/rwth_ient_logo@2x.png\" style=\"float: right;height: 5em;\">\n",
    "</div>\n",
    "\n",
    "\n",
    "# Reale Abtastung\n",
    "\n",
    "Zum Starten: Im Menü: Run <span class=\"fa-chevron-right fa\"></span> Run All Cells auswählen.\n",
    "\n",
    "## Einleitung\n",
    "\n",
    "Im Gegensatz zur [idealen Abtastung](GDET3%20Ideale%20Abtastung.ipynb) werden hier zwei Verfahren zur realen Abtastung betrachtet. Tatsächlich kann nicht mit einem idealen Dirac an einem definierten Zeitpunkt abgetastet werden. Im Folgenden werden zwei Verfahren der Abtastung, die *Shape-top Abtastung* und die *Flat-top Abtastung* beschrieben.\n",
    "\n",
    "## Shape-top Abtastung\n",
    "\n",
    "Bei der Shape-top Abtastung wird das kontinuierliche Signal $s(t)$ mit Abstand $T=\\frac{1}{r}$ abgetastet. \n",
    "Anstatt einer Diracfolge wird eine Folge schmaler Rechteckimpulse mit endlicher Dauer $T_0$ verwendet. Das abgetastete Signal $s_0(t)$ ergibt sich zu\n",
    "\n",
    "$$\n",
    "s_0(t) \n",
    "= s(t) \\cdot \\sum\\limits_{n=-\\infty}^\\infty \\mathrm{rect}\\left(\\frac{t-nT}{T_0}\\right) \n",
    "= s(t) \\cdot \\left[\\mathrm{rect}\\left(\\frac{t}{T_0}\\right) \\ast \\sum\\limits_{n=-\\infty}^\\infty \\delta(t-nT) \\right].\n",
    "$$\n",
    "\n",
    "Im Frequenzbereich folgt durch Transformation\n",
    "$$\n",
    "S_0(f) \n",
    "= S(f) \\ast \\left[T_0 \\mathrm{si}\\left(\\pi T_0 f\\right) \\cdot \\frac{1}{T}\\sum_{k=-\\infty}^\\infty \\delta(f-kr)\\right]\n",
    "=\n",
    "S(f) \\ast \\left[\\frac{T_0}{T} \\sum_{k=-\\infty}^\\infty \\delta\\left(f-\\frac{k}{T}\\right) \\mathrm{si} \\left(\\pi T_0 \\frac{k}{T}\\right) \\right]\\text{.}\n",
    "$$\n",
    "\n",
    "Auch hier entstehen wie bei der idealen Abtastung spektrale Kopien, welche um $\\frac{k}{T}$ zentriert sind. Jede spektrale Kopie wird mit einem von $f$ unabhängigen Faktor $\\mathrm{si} \\left(\\pi T_0 \\frac{k}{T}\\right)$ skaliert.\n",
    "\n",
    "Der Grenzübergang $T_0\\rightarrow 0$ liefert hier die ideale Abtastung: $\\lim\\limits_{T_0\\rightarrow 0}\\left(\\frac{1}{T_0}s_0(t)\\right) = s_\\mathrm{a}(t)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Flat-top Abtastung\n",
    "Bei der Flat-top Abtastung wird in Intervallen endlicher Dauer abgetastet und der Signalwert dann um $T=\\frac{1}{r}$ gehalten. Dieses Verfahren wird häufig in Analog-Digital-Wandlern eingesetzt. Das abgetastete Signal $s_0(t)$ ergibt sich somit zu\n",
    "\n",
    "$$\n",
    "s_0(t) \n",
    "= \\sum\\limits_{n=-\\infty}^\\infty s(nT) \\mathrm{rect}\\left(\\frac{t}{T}-\\frac{1}{2}-nT\\right)\n",
    "= s_\\mathrm{a}(t) \\ast \\mathrm{rect}\\left(\\frac{t}{T}-\\frac{1}{2}\\right)\n",
    "= \\left[s(t) \\cdot \\sum\\limits_{n=-\\infty}^\\infty \\delta(t-nT)\\right] \\ast \\mathrm{rect}\\left(\\frac{t}{T}\\right)\\ast \\delta\\left(t-\\frac{T}{2}\\right) \\text{.}\n",
    "$$\n",
    "\n",
    "Im Frequenzbereich folgt dann\n",
    "$$\n",
    "S_0(f) \n",
    "= S_\\mathrm{a}(f) \\cdot T \\cdot \\mathrm{si}(\\pi f T) \\cdot \\mathrm{e}^{-\\mathrm{j}\\pi f T}\n",
    "= \\left[S(f) \\ast \\sum\\limits_{k=-\\infty}^\\infty \\delta(f-kr)\\right] \\cdot \\mathrm{si}(\\pi f T) \\cdot \\mathrm{e}^{-\\mathrm{j}\\pi f T}\\text{.}\n",
    "$$\n",
    "\n",
    "In der Demonstration kann der Unterschied zwischen der Shape-top und der Flat-top Abtastung noch einmal anschaulich betrachtet werden."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Beispiel\n",
    "### Abtastung\n",
    "\n",
    "Zunächst wird noch einmal die [ideale Abtastung](GDET3%20Ideale%20Abtastung.ipynb) wiederholt. In der Abbildung ist als gestrichelte Linie das Signal $s(t)$ dargestellt, das zugehörige Spektrum in blau. Das Signal wird mit Abtastrate $r=2$ abgetastet.\n",
    "Das abgetastete Signal \n",
    "$s_\\mathrm{a}(t) = \\sum\\limits_{n=-\\infty}^\\infty s(nT)\\cdot\\delta(t-nT)$ und das Spektrum des abgetasteten Signals \n",
    "$S_\\mathrm{a}(f) = \\frac{1}{T} \\sum\\limits_{k=-\\infty}^\\infty S(f-kr)$ sind ebenfalls abgebildet. \n",
    "\n",
    "Das Spektrum des abgetasteten Signals zeigt die für die Abtastung typischen spektralen Wiederholungen. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# Construction of s(t) and corresponding spectrum S(f)\n",
    "t,deltat = np.linspace(-10,10,50001, retstep=True) # t-axis\n",
    "f,deltaf = np.linspace(-50,50,len(t), retstep=True) # f-axis\n",
    "\n",
    "F = 0.9 # frequency of the signal\n",
    "s = lambda t: signals_t['cos-Funktion'](t*F); \n",
    "S = lambda f: signals_f['cos-Funktion'](f, F);\n",
    "\n",
    "# Ideal sampling: Construction of sa(t) and Sa(f)\n",
    "r = 2; T = 1/r; # sampling rate\n",
    "\n",
    "## Time domain\n",
    "nT, snT = rwth_transforms.sample(t, s(t), T)\n",
    "\n",
    "## Frequency domain\n",
    "Sa = np.zeros_like(S(f))\n",
    "kMax = 16 # number of k values in sum for Sa(f), should be infinity :)\n",
    "for k in np.arange(-kMax, kMax+1): # evaluate infinite sum only for 2*kMax+1 elements \n",
    "    Sa += S(f-k/T)\n",
    "Sa = Sa/T\n",
    "fSadirac = f[np.where(Sa)]; Sadirac = Sa[np.where(Sa)]\n",
    "\n",
    "# Plot\n",
    "## Time domain\n",
    "fig, axs = plt.subplots(2,1, **rwth_plots.landscape)\n",
    "ax = axs[0]; ax.set_title('Zeitbereich');\n",
    "ax.plot(t, s(t), color='rwth:blue', linestyle='--', label=r'$s(t)$');\n",
    "rwth_plots.plot_dirac(ax, nT, snT, 'rwth:red', label=r'$s_\\mathrm{a}(t)$')\n",
    "ax.set_xlabel(r'$\\rightarrow t$'); ax.set_xlim([-7.5,7.5]); ax.legend(loc=2); rwth_plots.grid(ax); rwth_plots.axis(ax);\n",
    "## Frequency domain\n",
    "ax = axs[1];  ax.set_title('Frequenzbereich');\n",
    "rwth_plots.plot_dirac(ax, fSadirac, Sadirac, 'rwth:red', label=r'$S_\\mathrm{a}(f)$');\n",
    "rwth_plots.plot_dirac(ax, f[np.where(S(f))], S(f)[np.where(S(f))], 'rwth:blue', label=r'$S(f)$');\n",
    "ax.set_xlim([-7.5,7.5]); ax.legend(loc=2); ax.set_xlabel(r'$\\rightarrow f$'); rwth_plots.grid(ax); rwth_plots.axis(ax);\n",
    "txt,_=rwth_plots.annotate_xtick(ax, r'$r=2$', r, -.15, 'black'); txt.get_bbox_patch().set_alpha(1);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nun wird das abgetastete Signal $s_0(t)$ im Zeitbereich betrachtet, welches mittels Flat-top Abtastung erzeugt wurde:\n",
    "$$\n",
    "s_0(t) \n",
    "= s_\\mathrm{a}(t) \\ast \\mathrm{rect}\\left(\\frac{t}{T}-\\frac{1}{2}\\right)\n",
    "= \\sum\\limits_{n=-\\infty}^\\infty s(nT) \\mathrm{rect}\\left(\\frac{t}{T}-\\frac{1}{2}-nT\\right).\n",
    "$$\n",
    "\n",
    "Die Abtastrate ist ebenfalls $r=2$.\n",
    "Für die Flat-top Abtastung charakteristisch wird der Signalwert bis zum nächsten Abtastwert gehalten. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# Time-Domain\n",
    "s0 = np.zeros_like(t) # sum of rects\n",
    "Nmax = np.ceil(t[-1]/T) # Parts of the infinite rect sum, should be infinity :)\n",
    "for n in np.arange(-Nmax,Nmax+1):\n",
    "    s0 = s0 + rect((t-n*T)/T-0.5) * s(n*T)\n",
    "\n",
    "# Plot\n",
    "fig, ax = plt.subplots(**rwth_plots.landscape); ax.set_title('Zeitbereich')\n",
    "ax.plot(t, s(t), 'rwth:blue', linestyle='--', label=r'$s(t)$'); ax.plot(t, s0, 'rwth:red', label=r'$s_0(t)$')\n",
    "ax.set_xlabel(r'$\\rightarrow t$'); ax.set_xlim([-2.9, 2.9]); ax.legend(loc=2); rwth_plots.grid(ax); rwth_plots.axis(ax);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dies hat im Frequenzbereich zur Konsequenz, dass die spektralen Kopien mit $T \\mathrm{si}(\\pi f T) \\cdot \\mathrm{e}^{-j\\pi f T}$ gewichtet werden. \n",
    "$S_0(f)$ ergibt sich also zu\n",
    "$$\n",
    "S_0(f) \n",
    "= S_\\mathrm{a}(f) \\cdot T \\mathrm{si}(\\pi f T) \\cdot \\mathrm{e}^{-j\\pi f T}.\n",
    "$$\n",
    "Die nachfolgende Abbildung verdeutlicht diesen Effekt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# Frequency domain\n",
    "S_si = T*si(np.pi*T*f)\n",
    "S_exp = np.exp(-1j*np.pi*f*T)\n",
    "S0 = Sa * S_si * S_exp\n",
    "\n",
    "# Plot\n",
    "## Magnitude\n",
    "fig, axs = plt.subplots(2,1, **rwth_plots.landscape); \n",
    "ax = axs[0]; ax.set_title('Betrag')\n",
    "ax.plot(f, np.abs(S_si*S_exp), 'k--', label=r'$|T\\mathrm{si}(\\pi f T)e^{-\\mathrm{j}\\pi f T}|$')\n",
    "fS0dirac = f[np.where(S0)];   S0dirac = S0[np.where(S0)] # sample S0 and f\n",
    "rwth_plots.plot_dirac(ax, fS0dirac, np.abs(S0dirac), 'rwth:red', label=r'$|S_0(f)$|');\n",
    "ax.set_xlim([-7.5,7.5]); ax.legend(loc=2); ax.set_xlabel(r'$\\rightarrow f$'); rwth_plots.grid(ax); rwth_plots.axis(ax);\n",
    "## Phase\n",
    "ax = axs[1]; ax.set_title('Phase')\n",
    "ax.plot(f, np.angle(S_si*S_exp), 'k--', label=r'$\\angle T\\mathrm{si}(\\pi f T)e^{-\\mathrm{j}\\pi f T}$')\n",
    "rwth_plots.stem(ax, fS0dirac, np.angle(S0dirac), 'rwth:red', label=r'$\\angle S_0(f)$')\n",
    "ax.set_xlim([-7.5,7.5]); ax.legend(loc=2); ax.set_xlabel(r'$\\rightarrow f$'); rwth_plots.grid(ax); rwth_plots.axis(ax);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Rekonstruktion\n",
    "\n",
    "Durch die Multiplikation von $S_\\mathrm{a}(f)$ mit $T \\cdot \\mathrm{si}(\\pi f T) \\cdot \\mathrm{e}^{-j\\pi f T}$ wird das Spektrum $S_0(f)$ im Basisband verzerrt. So ist es nicht möglich, $S(f)$ mittels eines einfachen idealen Tiefpasses zu rekonstruieren. Zusätzlich ist ein Filter nötig, welches diesen Faktor ausgleicht, dieses ist in der folgenden Abbildung dargestellt.\n",
    "$$\n",
    "H_\\mathrm{eq}(f) = \\frac{1}{T \\cdot \\mathrm{si}(\\pi f T) \\cdot \\mathrm{e}^{-j\\pi f T}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# Reconstruction filters #plt.close('all')\n",
    "## Ideal Low pass to crop the base band\n",
    "H_lp = rect(f/(r+0.001)) # ideal low pass between -r/2 and r/2\n",
    "## Equalizing filter to compensate the influence of si(...) and exp(...) terms in S_0(f)\n",
    "H_eq = 1/(T*si(np.pi*T*f) * np.exp(-1j*np.pi*f*T))\n",
    "## Overall reconstruction filter\n",
    "H = H_lp * H_eq\n",
    "\n",
    "# Plot\n",
    "fig,axs = plt.subplots(2,1, **rwth_plots.landscape)\n",
    "ax = axs[0]\n",
    "ax.plot(f, np.abs(H_eq), 'k--', linewidth=1, label=r'$|H_\\mathrm{eq}(f)|$')\n",
    "ax.plot(f, np.abs(H), 'k', label=r'$|H(f)|$')\n",
    "ax.set_xlim([-7.5,7.5]); ax.legend(loc=2); ax.set_ylim([-.1,21]);\n",
    "ax.set_xlabel(r'$\\rightarrow f$'); rwth_plots.grid(ax); rwth_plots.axis(ax);\n",
    "\n",
    "ax = axs[1]\n",
    "ax.plot(f, np.angle(H_eq), 'k--', linewidth=1, label=r'$\\angle H_\\mathrm{eq}(f)$')\n",
    "ax.plot(f, np.angle(H)*H_lp, 'k', label=r'$\\angle H(f)$')\n",
    "ax.set_xlim([-7.5,7.5]); ax.legend(loc=2); #ax.set_ylim([-.1,11]);\n",
    "ax.set_xlabel(r'$\\rightarrow f$'); rwth_plots.grid(ax); rwth_plots.axis(ax);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dieses Filter wird nun zur Rekonstruktion angewendet. \n",
    "Das rekonstruierte Signal unter Verwendung des ausgleichenden Filters ist unten abgebildet. Das Originalsignal konnte erfolgreich rekonstruiert werden. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "G = S0 * H*T; \n",
    "g = rwth_transforms.idft(G); g = np.fft.ifftshift(np.real(g)); # IDFT\n",
    "G = np.real(G); # discards small imaginary numbers close to zero\n",
    "\n",
    "# Plot\n",
    "fig, axs = plt.subplots(2, 1, **rwth_plots.landscape)\n",
    "ax = axs[0]; fGdirac  = f[np.where(G)]; Gdirac = G[np.where(G)]\n",
    "rwth_plots.plot_dirac(ax, fGdirac, Gdirac, 'rwth:green');\n",
    "ax.set_xlabel(r'$\\rightarrow f$'); ax.set_ylabel(r'$\\uparrow G(f)$', bbox=rwth_plots.wbbox);\n",
    "ax.set_xlim([-7.5,7.5]); rwth_plots.grid(ax); rwth_plots.axis(ax);\n",
    "\n",
    "ax = axs[1]; ax.plot(t, s(t), color='rwth:blue', linestyle='--', label=r'$s(t)$');\n",
    "ax.plot(t/deltat/(f[-1]*2), g, color='rwth:green', linestyle='-', label=r'$g(t)$');\n",
    "ax.set_xlabel(r'$\\rightarrow t$'); ax.set_xlim([-7.5,7.5]); ax.legend(loc=2); rwth_plots.grid(ax); rwth_plots.axis(ax);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Demo\n",
    "In der folgenden interaktiven Demonstration kann nun betrachtet werden, was der Unterschied zwischen der Flat-top und der Shape-top Abtastung ist. \n",
    "Dargestellt sind untereinander:\n",
    "* der Zeitbereich mit dem Originalsignal und dem abgestasteten Signal\n",
    "* der zugehörige Frequenzbereich mit dem Originalspektrum, dem Spektrum des abgetasteten Signal und dem Rekonstruktionsfilter\n",
    "* das rekonstruierte Spektrum\n",
    "* das rekonstruierte Signal im Zeitbereich.\n",
    "\n",
    "Im Drop-Down-Menü kann die Art der Abtastung (Shape-top oder Flat-top) sowie die abzutastende Funktion (Cosinus-, Sinus-und si-Funktion, sowie Rechteck- oder Dreieckimpuls) ausgewählt werden. \n",
    "Über den Schieberegler kann $F$ geändert werden, was bei Cosinus-, Sinus- und si-Funktion die Frequenz und bei Rechteck- und Dreieckimpuls die Breite beeinflusst. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "T0 = T/4 # width of rects for shape-top sampling\n",
    "\n",
    "fig, axs = plt.subplots(4, 1, **rwth_plots.landscape); plt.tight_layout();\n",
    "@widgets.interact(sampling_type=widgets.Dropdown(options=['Shape-top', 'Flat-top'], description=r'Art der Abtastung:', style=rwth_plots.wdgtl_style),\n",
    "                  s_type=widgets.Dropdown(options=list(signals_t.keys()), description=r'Wähle $s(t)$:'),\n",
    "                  F=widgets.FloatSlider(min=0.1, max=2, value=0.9, step=.1, description=r'$F$', style=rwth_plots.wdgtl_style, continuous_update=False))\n",
    "def update_plots(sampling_type, s_type, F):\n",
    "    s = lambda t: signals_t[s_type](t*F); \n",
    "    S = lambda f: signals_f[s_type](f, F);\n",
    "    nT, snT = rwth_transforms.sample(t, s(t), T)\n",
    "    \n",
    "    # Construct sampled signal\n",
    "    if sampling_type == 'Shape-top':\n",
    "        u = np.zeros_like(t) # sum of rects\n",
    "        for n in np.arange(-Nmax,Nmax+1):\n",
    "            u = u + rect((t-n*T)/T0)\n",
    "        s0 = s(t) * u\n",
    "    elif sampling_type == 'Flat-top':\n",
    "        s0 = np.zeros_like(t) # sum of rects\n",
    "        for n in np.arange(-Nmax,Nmax+1):\n",
    "            s0 = s0 + rect((t-n*T)/T-0.5) * s(n*T)\n",
    "    \n",
    "    # Construct sampled spectrum\n",
    "    if sampling_type == 'Shape-top':\n",
    "        S0 = np.zeros_like(S(f))\n",
    "        for k in np.arange(-kMax, kMax+1): # evaluate infinite sum only for 2*kMax+1 elements \n",
    "            S0 += S(f-k/T) * si(np.pi*T0*k/T)\n",
    "        S0 = S0*T0/T\n",
    "    elif sampling_type == 'Flat-top':\n",
    "        Sa = np.zeros_like(S(f))\n",
    "        for k in np.arange(-kMax, kMax+1): # evaluate infinite sum only for 2*kMax+1 elements \n",
    "            Sa += S(f-k/T)\n",
    "        S0 = Sa * si(np.pi*T*f) * np.exp(-1j*np.pi*f*T)\n",
    "        \n",
    "    # Reconstruct g(t)\n",
    "    if sampling_type == 'Shape-top':\n",
    "        H = H_lp\n",
    "        G = S0 * H * T / T0;\n",
    "    elif sampling_type == 'Flat-top':\n",
    "        H = H_lp * H_eq * T\n",
    "        G = S0 * H\n",
    "    g = rwth_transforms.idft(G); \n",
    "    g = np.fft.ifftshift(np.real(g)); # IDFT\n",
    "        \n",
    "    # Sample for plot\n",
    "    if s_type == 'cos-Funktion' or s_type == 'sin-Funktion':\n",
    "        fS0dirac = f[np.where(S0)];   S0dirac = S0[np.where(S0)]\n",
    "        fSdirac  = f[np.where(S(f))]; Sdirac  = S(f)[np.where(S(f))]\n",
    "        fGdirac  = f[np.where(G)]; Gdirac = G[np.where(G)]\n",
    "        if s_type == 'sin-Funktion':\n",
    "            Sdirac = np.imag(Sdirac); S = lambda f: np.imag(signals_f[s_type](f, F)); \n",
    "            G = np.imag(G); Gdirac = np.imag(Gdirac)\n",
    "        else:\n",
    "            Sdirac = np.real(Sdirac); S0 = np.real(S0); G = np.real(G); Gdirac = np.real(Gdirac)\n",
    "    else:\n",
    "        g /= (len(f)/(2*f[-1])) # Parseval :)\n",
    "        S0dirac = np.zeros_like(f)\n",
    "        G = np.real(G)\n",
    "        \n",
    "    if sampling_type == 'Shape-top': # normalize to T0 for plotting reasons\n",
    "        S0 = S0/T0\n",
    "        S0dirac = S0dirac/T0\n",
    "    \n",
    "    # Plot\n",
    "    if not axs[0].lines: # Call plot() and decorate axes. Usually, these functions take some processing time\n",
    "        ax = axs[0]; ax.set_title('Zeitbereich');\n",
    "        ax.plot(t, s(t), 'rwth:blue', linestyle='--', label=r'$s(t)$'); \n",
    "        ax.plot(t, s0, 'rwth:red', label=r'$s_0(t)$')\n",
    "        ax.set_xlabel(r'$\\rightarrow t$'); \n",
    "        ax.set_xlim([-2.9, 2.9]); ax.legend(loc=2); rwth_plots.grid(ax); rwth_plots.axis(ax);\n",
    "        \n",
    "        ax = axs[1];  ax.set_title('Frequenzbereich');\n",
    "        ax.plot(f, np.abs(H), '-', color='rwth:black', label=r'$|H(f)|$')\n",
    "        if s_type == 'cos-Funktion' or s_type == 'sin-Funktion':\n",
    "            ax.plot(f, np.ones_like(f)*np.nan, '-', color='rwth:red', label=r'$|S_0(f)|$');\n",
    "            rwth_plots.plot_dirac(ax, fS0dirac, np.abs(S0dirac), 'rwth:red');\n",
    "        else:\n",
    "            ax.plot(f, np.abs(S0), '-', color='rwth:red', label=r'$|S_0(f)|$'); \n",
    "            rwth_plots.plot_dirac(ax, [], [], 'rwth:red');\n",
    "        ax.plot(f, S(f), color='rwth:blue', linestyle='--', linewidth=1, label=r'$S(f)$')\n",
    "        ax.set_xlim([-7.5,7.5]); ax.set_ylim([-1,2]); ax.legend(loc=2);\n",
    "        ax.set_xlabel(r'$\\rightarrow f$'); rwth_plots.grid(ax); rwth_plots.axis(ax);\n",
    "        txt,_=rwth_plots.annotate_xtick(ax, r'$r=2$', r, -.15, 'rwth:black'); txt.get_bbox_patch().set_alpha(1);\n",
    "        txt,_=rwth_plots.annotate_xtick(ax, r'$f_\\mathrm{g}$', r/2, -.15, 'rwth:black'); txt.get_bbox_patch().set_alpha(1);\n",
    "        \n",
    "        ax = axs[2];\n",
    "        if s_type == 'cos-Funktion' or s_type == 'sin-Funktion':\n",
    "            ax.plot(f, np.ones_like(f)*np.nan, '-', color='rwth:green'); \n",
    "            rwth_plots.plot_dirac(ax, fGdirac, Gdirac, 'rwth:green');\n",
    "        else:\n",
    "            ax.plot(f, G, '-', color='rwth:green'); \n",
    "            rwth_plots.plot_dirac(ax, [], [], 'rwth:red');\n",
    "        ax.set_xlabel(r'$\\rightarrow f$'); ax.set_ylabel(r'$\\uparrow G(f)$', bbox=rwth_plots.wbbox);\n",
    "        ax.set_xlim([-7.5,7.5]); rwth_plots.grid(ax); rwth_plots.axis(ax);\n",
    "        \n",
    "        ax = axs[3]; ax.set_title('Zeitbereich (nach Rekonstruktion)');\n",
    "        ax.plot(t, s(t), color='rwth:blue', linestyle='--', label=r'$s(t)$');\n",
    "        ax.plot(t/deltat/(f[-1]*2), g, 'rwth:green', label=r'$g(t)$');\n",
    "        ax.set_xlabel(r'$\\rightarrow t$'); ax.set_xlim([-7.5,7.5]); ax.legend(loc=2); rwth_plots.grid(ax); rwth_plots.axis(ax);\n",
    "        plt.tight_layout()\n",
    "    else:\n",
    "        axs[0].lines[0].set_ydata(s(t)); axs[0].lines[1].set_ydata(s0); axs[1].lines[-3].set_ydata(S(f)); \n",
    "        axs[1].lines[0].set_ydata(np.abs(H))\n",
    "        if s_type == 'cos-Funktion' or s_type == 'sin-Funktion': # dirac plot\n",
    "            rwth_plots.dirac_set_data(axs[1].containers, fS0dirac, np.abs(S0dirac));\n",
    "            axs[1].lines[1].set_ydata(np.ones_like(f)*np.nan); \n",
    "            rwth_plots.dirac_set_data(axs[2].containers, fGdirac, Gdirac);   axs[2].lines[0].set_ydata(np.ones_like(f)*np.nan);\n",
    "        else:\n",
    "            axs[1].lines[1].set_ydata(np.abs(S0)); rwth_plots.dirac_set_data(axs[1].containers, [], []); \n",
    "            axs[2].lines[0].set_ydata(G); rwth_plots.dirac_set_data(axs[2].containers, [], []);\n",
    "            \n",
    "        axs[3].lines[0].set_ydata(s(t)); axs[3].lines[1].set_ydata(g);\n",
    "        \n",
    "        if s_type == 'sin-Funktion': # Adapt labels\n",
    "            axs[1].lines[-3].set_label(r'$\\mathrm{Im}\\{S(f)\\}$');\n",
    "            axs[2].yaxis.label.set_text(r'$\\uparrow \\mathrm{Im}\\{G(f)\\}$');\n",
    "        else:\n",
    "            axs[1].lines[-3].set_label(r'$S(f)$')\n",
    "            axs[2].yaxis.label.set_text(r'$\\uparrow G(f)$');\n",
    "        axs[1].legend(loc=2)\n",
    "        \n",
    "    tmp = np.concatenate((np.abs(S0),np.abs(S0dirac),np.abs(H))); rwth_plots.update_ylim(axs[1], tmp, 0.19, np.max(np.abs(tmp))); rwth_plots.update_ylim(axs[2], G, 0.19, np.max(G));\n",
    "    rwth_plots.update_ylim(axs[3], np.concatenate((s(t),g)), 0.19, np.max(np.concatenate((s(t),g))));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Aufgaben\n",
    "Wähle eine beliebige Funktion aus.\n",
    "* Wie sieht das abgetastete Signal mit Shape-top Abtastung aus und wie mit Flat-top Abtastung?\n",
    "* Welche Auswirkungen hat die Abtastungsart auf das Spektrum und die Rekonstruktion?\n",
    "* Variiere $F$. Was passiert?\n",
    "* Führe diese Beobachtung für unterschiedliche Funktionen aus. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___\n",
    "This notebook is provided as [Open Educational Resource](https://en.wikipedia.org/wiki/Open_educational_resources) (OER). Feel free to use the notebook for your own purposes. The code is licensed under the [MIT license](https://opensource.org/licenses/MIT). \n",
    "\n",
    "Please attribute the work as follows: \n",
    "*Christian Rohlfing, Übungsbeispiele zur Vorlesung \"Grundgebiete der Elektrotechnik 3 - Signale und Systeme\"*, gehalten von Jens-Rainer Ohm, 2020, Institut für Nachrichtentechnik, RWTH Aachen University."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
