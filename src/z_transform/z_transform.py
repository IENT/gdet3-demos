from ipywidgets import interactive, HBox, VBox
import ipywidgets as widgets
from IPython.display import display

import numpy as np
import bisect
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt

import rwth_nb.plots.mpl_decorations as rwth_plots
import rwth_nb.misc.transforms as rwth_transforms


class pzPoint():
    h_point = None
    h_point_conj = None
    h_order = None

    def __init__(self, p=0 + 0j, order=1):
        # super().__init__(*args, **kwargs)
        self.p = p
        self.order = order

    def clear(self):
        if self.h_point is not None:
            self.h_point.remove()
            self.h_point = None
        if self.h_point_conj is not None:
            self.h_point_conj.remove()
            self.h_point_conj = None
        if self.h_order is not None:
            self.h_order[0].remove()
            self.h_order[1].remove()
            self.h_order = None


class zPlot():
    pp = np.array([])
    pz = np.array([])
    roc = {'sigma': [0, 0], 'zroc': None, 'inf': 12}
    qdelta = 0.1
    qthresh = 0.1
    fig = None
    ax = None
    handles = {'ph': None, 'axh': None, 'pH': None, 'axH': None}
    # TODO - add more filter types
    filter_types = {
        'Manuell': 'man', 'Sprungfunktion': 'unit', 'Sinus': 'sin', 'Cosinus': 'cos'
    }
    mode = 'p'
    mode_types = {'Polstelle': 'p', 'Nullstelle': 'z'}
    action = 'add'
    action_locked = False
    action_types = {'Hinzufügen': 'add', 'Löschen': 'del', 'Konvergenzbereich ändern': 'roc'}
    H = None
    Hlog = None

    def __init__(self, fig, pp=np.array([0]), pz=np.array([]), ord_p=np.array([1]), ord_z=np.array([])):

        self.H0 = 1
        # self.filter = 'unit'

        self.H0_txt = None
        self.systemIsStable = True

        self.length = 31
        self.n = np.linspace(-15, 15, num=self.length)
        self.f = np.linspace(-15, 15, num=1024)

        self.open_figure(fig)

        # Poles
        return
        self.pp = np.array([])
        if pp.size != 0 and ord_p.size == 0:
            ord_p = np.ones(pp.size)
        else:
            ord_p = np.array(ord_p)

        for i, p in enumerate(pp):
            self.update_point(p, 'p', ord_p[i])

        # Zeroes
        self.pz = np.array([])
        if pz.size != 0 and ord_z.size == 0:
            ord_z = np.ones(pz.size)
        else:
            ord_z = np.array(ord_z)

        for i, p in enumerate(pz):
            self.update_point(p, 'z', ord_z[i])

        self.update_roc(1)

    def open_figure(self, fig):

        gs = gridspec.GridSpec(2, 2, left=0, wspace=0.1, figure=fig)

        # First axis for plotting s and h
        # axes limits and labels
        ax = plt.subplot(gs[:, 0])

        ax.set_xlim(-6, 6)
        ax.set_ylim(-6, 6)
        ax.set_xlabel(r'$\rightarrow \mathrm{Re}$')
        ax.set_ylabel(r'$\uparrow \mathrm{Im}$')
        rwth_plots.axis(ax)
        self.fig = fig
        self.ax = ax
        self.ax.set_title('Pol- /Nullstellen Diagramm', fontsize='12')
        self.ax.set_aspect('equal')

        # * move indicates whether a drag is happening
        # * selected_coords indicates selected coords when button is pressed
        # * ghost_hpoint, ghost_hpoint_conj are the half transparent poles/zeroes to indicate where the newly created
        # will be
        self.move = False
        self.selected_coords = None
        self.ghost_hpoint = None
        self.ghost_hpoint_conj = None

        def on_btn_press(event):
            if event.inaxes != self.ax:
                return
            if self.filter != 'man':
                return

            # button press, reset move
            self.move = False

            # find closest point and set selected if there are any valid points nearby
            p = event.xdata + 1j * event.ydata
            self.selected_coords = p

        def on_btn_release(event):
            if event.inaxes != self.ax:
                if self.ghost_hpoint is not None:
                    self.ghost_hpoint.remove()
                    self.ghost_hpoint_conj.remove()
                    self.ghost_hpoint = None
                    self.ghost_hpoint_conj = None
                return
            if self.filter != 'man':
                return

            # press + no movement + release = click
            if not self.move:
                on_click(event)

            # press + movement + release = drag
            # if dragging process is finished
            if self.move and self.find_closest_point(self.selected_coords, self.mode)[0] is not None:
                # release move
                self.move = False

                # remove any ghosts from the figure and clear variables
                if self.ghost_hpoint is not None:
                    self.ghost_hpoint.remove()
                    self.ghost_hpoint_conj.remove()
                    self.ghost_hpoint = None
                    self.ghost_hpoint_conj = None

                # delete at press selected point
                tmp_action = self.action
                self.action = 'del'
                self.update_point(self.selected_coords, self.mode)

                # add new point at drag point
                self.action = 'add'
                on_click(event)
                self.action = tmp_action

        def on_motion(event):
            if event.inaxes != self.ax:
                return
            if self.filter != 'man':
                return

            # if button is pressed
            if event.button == 1:
                # lock move
                self.move = True

                # if a point was selected
                if self.find_closest_point(self.selected_coords, self.mode)[0] is not None:
                    if self.ghost_hpoint is None:
                        # draw ghost point
                        if self.mode == 'p':
                            plotstyle = rwth_plots.style_poles
                        else:
                            plotstyle = rwth_plots.style_zeros

                        self.ghost_hpoint, = self.ax.plot(event.xdata, event.ydata, **plotstyle, alpha=.5)

                        if np.abs(event.ydata) > 0:  # plot complex conjugated poles/zeroes
                            self.ghost_hpoint_conj, = self.ax.plot(event.xdata, -event.ydata, **plotstyle, alpha=.5)
                    else:
                        self.ghost_hpoint.set_data(event.xdata, event.ydata)
                        self.ghost_hpoint_conj.set_data(event.xdata, -event.ydata)

        def on_click(event):
            if self.filter != 'man':
                return
            if event.inaxes != self.ax:
                return

            # update point
            p = event.xdata + 1j * event.ydata
            self.update_point(p, self.mode)

        self.fig.canvas.mpl_connect('button_press_event', on_btn_press)
        self.fig.canvas.mpl_connect('button_release_event', on_btn_release)
        self.fig.canvas.mpl_connect('motion_notify_event', on_motion)

        self.handles['axh'] = plt.subplot(gs[0, 1])
        self.handles['axh'].set_xlim(-15, 15)
        self.handles['axh'].set_title('Impulsantwort', fontsize='12')
        self.handles['axh'].set_xlabel(r'$\rightarrow n$')
        self.handles['axh'].set_ylabel(r'$\uparrow h(n)$')
        self.handles['containerh'] = rwth_plots.stem(self.handles['axh'], self.n, self.n)
        rwth_plots.axis(self.handles['axh'])

        self.pbzErrortxt = self.handles['axh'].text(
            -13, .0125,
            'Zählergrad größer als Nennergrad. \nKeine Partialbruchzerlegung möglich!',
            fontsize=12, color='rwth:red', visible=False, bbox=rwth_plots.wbbox
        )

        self.handles['axH'] = plt.subplot(gs[1, 1])
        self.handles['axH'].set_title('Übertragungsfunktion', fontsize='12')
        self.handles['axH'].set_xlabel(r'$\rightarrow f$')
        self.handles['axH'].set_ylabel(r'$\uparrow |H_\mathrm{a}(f)|$ [dB]')
        self.stabilitytxt = self.handles['axH'].text(
            -7.5, 0,
            'Das System ist nicht stabil!',
            fontsize=12, color='rwth:red', visible=False, bbox=rwth_plots.wbbox
        )
        self.handles['lineH'], = self.handles['axH'].plot(self.f, self.f)
        rwth_plots.axis(self.handles['axH'])
        self.handles['axH'].spines['bottom'].set_position(('outward', 0))

        # Widgets
        self.widgets_locked = True
        self.w_filter_type = interactive(self.update_filter, filtr=widgets.Dropdown(
            options=list(self.filter_types.keys()), value="Sprungfunktion", description='Filter'
        ))
        self.w_action_type = interactive(self.update_action, action=widgets.Dropdown(
            options=list(self.action_types.keys()), value="Hinzufügen", description='Modus', disabled=True
        ))
        self.w_point_type = interactive(self.update_mode, mode=widgets.Dropdown(
            options=list(self.mode_types.keys()), value="Polstelle", description='Typ', disabled=True
        ))
        self.w_amp_type = interactive(self.update_amp, H0=widgets.IntSlider(
            min=1, max=10, step=1, value=1, description="$H_0$"
        ))
        self.w_no_of_poles = interactive(self.update_no_of_poles, P=widgets.IntSlider(
            min=1, max=7, step=1, value=1, disabled=True, description="$P$"))

        self.VBox_AMP = VBox([self.w_filter_type, self.w_amp_type])  # vertical box for displaying H0
        # self.VBox_Butter = VBox([self.w_filter_type, self.w_no_of_poles])  # vertical box for displaying no. of poles
        # (only for butterworth)
        self.VBox_mode_type = VBox([self.w_action_type, self.w_point_type])  # vertical box action type and point type

        self.w_hbox = HBox([self.VBox_AMP, self.VBox_mode_type])

        self.widgets_locked = False
        self.update_filter('Sprungfunktion')
        display(self.w_hbox)

    def find_closest_point(self, p, mode='n'):
        if mode == 'n':
            mode = self.mode

        # Check if point is already there
        if mode == 'p':
            points = self.pp
        else:
            points = self.pz
        ps = np.array(list((x.p for x in points)))

        # Search for point by minimizing cost
        if ps.size > 0:  # some points already available
            C1 = np.abs(ps - p) ** 2
            C2 = np.abs(ps - p.conjugate()) ** 2  # check also for complex conjugate points
            C1min = min(C1, default=-1)
            C2min = min(C2, default=-1)

            if C1min <= C2min:
                ind = np.argmin(C1)
                Cmin = C1min
            else:
                ind = np.argmin(C2)
                Cmin = C2min

            # self.ax.set_xlabel('xdata=%f, ydata=%f' % (p.real, p.imag))

            if mode == 'p':
                pPoint = self.pp[ind]
            else:
                pPoint = self.pz[ind]

        else:  # no points added yet
            Cmin = self.qthresh * 2
            ind = -1

        if Cmin <= self.qthresh:
            pPoint = points[ind]
        else:
            pPoint = None

        return (pPoint, ind)

    def update_point(self, p, mode='n', neword=1):
        if self.action == 'roc':
            self.update_roc(np.abs(p))
            return

        if mode == 'n':
            mode = self.mode

        # Quantize point to grid and find closest point
        p = self.qdelta * round(p.real / self.qdelta) + 1j * self.qdelta * round(p.imag / self.qdelta)
        pPoint, ind = self.find_closest_point(p, mode)

        # Add new or delete point or update order
        if pPoint is None:
            if self.action == 'add':  # add new point
                pPoint = pzPoint(p, neword)
                if mode == 'p':
                    self.pp = np.append(self.pp, pPoint)
                    ind = self.pp.size - 1
                    plotstyle = rwth_plots.style_poles
                else:
                    self.pz = np.append(self.pz, pPoint)
                    ind = self.pz.size - 1
                    plotstyle = rwth_plots.style_zeros

                # Plot points
                pPoint.h_point, = self.ax.plot(p.real, p.imag, **plotstyle)
                if np.abs(p.imag) > 0:  # plot complex conjugated poles/zeroes
                    pPoint.h_point_conj, = self.ax.plot(p.real, -p.imag, **plotstyle)
            elif self.action == 'del':
                return

        else:  # delete / update order
            if self.action == 'add':  # increase order
                pPoint.order += 1

            elif self.action == 'del':

                if pPoint.order > 1:  # decrease order
                    pPoint.order -= 1
                else:  # delete point
                    pPoint.clear()
                    if mode == 'p':
                        self.pp = np.delete(self.pp, ind)
                    else:
                        self.pz = np.delete(self.pz, ind)

                    # return
                    # delete(pPoint)

        # plot / update order if necessary
        if self.action == 'add' or self.action == 'del':
            if pPoint.order == 1 and pPoint.h_order is not None:
                pPoint.h_order[0].remove()
                pPoint.h_order[1].remove()
                pPoint.h_order = None
            elif pPoint.order == 2:
                if pPoint.h_order is None:
                    align = 'left' if self.mode == 'p' else 'right'
                    pPoint.h_order = (
                        self.ax.text(
                            pPoint.p.real, abs(pPoint.p.imag), '({:1.0f})'.format(pPoint.order),
                            fontsize=12, horizontalalignment=align, verticalalignment='bottom'),
                        self.ax.text(
                            pPoint.p.real, -abs(pPoint.p.imag), '({:1.0f})'.format(pPoint.order),
                            fontsize=12, horizontalalignment=align, verticalalignment='bottom'))
                else:
                    pPoint.h_order[0].set_text('({:1.0f})'.format(pPoint.order))
                    pPoint.h_order[1].set_text('({:1.0f})'.format(pPoint.order))

            elif pPoint.order > 2:
                pPoint.h_order[0].set_text('({:1.0f})'.format(pPoint.order))
                pPoint.h_order[1].set_text('({:1.0f})'.format(pPoint.order))

        # elif self.action == 'roc':

        # plt.show()
        self.update_roc(np.mean(np.abs(self.roc['sigma'])))

    def update_roc(self, p=0):

        sigmas = np.sort(np.abs(np.array(list((x.p for x in self.pp)))))

        i = bisect.bisect_left(sigmas, p.real)
        if i >= sigmas.size:
            right = 12
        else:
            right = sigmas[i]

        if i == 0:
            left = 0
        else:
            left = sigmas[i - 1]

        self.roc['sigma'][0] = left
        self.roc['sigma'][1] = right
        xx = np.array([self.roc['sigma'][0], self.roc['sigma'][1]])

        if self.roc['zroc'] is not None:
            self.roc['zroc'].remove()

        self.roc['zroc'] = rwth_plots.plot_zroc(self.ax, xx)

        self.update_plot()

    def update_mode(self, mode):
        self.mode = self.mode_types[mode]

    def update_action(self, action):
        self.action = self.action_types[action]

    def update_amp(self, H0):
        self.H0 = H0

        try:
            self.H0_txt.remove()
        except AttributeError:
            pass

        self.H0_txt = self.ax.text(4.5, 5, r'$H_0$: ' + str(self.H0), fontsize=12, bbox=rwth_plots.wbbox)

        self.update_plot()

    def update_no_of_poles(self, P):
        self.P = P
        if self.w_filter_type.kwargs['filtr'] == 'Butterworth':  # skip execution on startup
            self.update_filter('Butterworth')

    def update_filter(self, filtr):
        if not self.widgets_locked:
            self.filter = self.filter_types[filtr]

            poles_butr = np.array([])
            ord_poles_butr = np.array([])

            # calculate butterworth poles and orders
            if filtr == 'Butterworth':
                # show slider for no of poles (filtergrad)
                # self.w_hbox.children = [self.VBox_Butter, self.VBox_mode_type]

                self.w_no_of_poles.children[0].disabled = False

                filtergrad = self.P
                filtergrenzfrequenz = 1  # omega_g = 1

                tmp = (np.linspace(0, filtergrad - 1, num=filtergrad) + 0.5) / filtergrad * np.pi + np.pi / 2
                tmp = tmp[np.where(tmp <= np.pi)[0]]

                poles_butr = np.array(filtergrenzfrequenz * np.exp(1j * tmp))
                ord_poles_butr = np.array(np.ones(poles_butr.shape), dtype=int)

            else:
                self.w_no_of_poles.children[0].disabled = False
                # show slider for H0
                # self.w_hbox.children = [self.VBox_AMP, self.VBox_mode_type]

            # clear plot
            def clear_all_points():
                tmp = list(self.pp) + list(self.pz)
                list(map(lambda p: p.clear(), tmp))
                self.pp = self.pz = np.array([])

            def update_widgets(filtr):
                self.w_action_type.children[0].disabled = filtr != 'Manuell'
                self.w_point_type.children[0].disabled = filtr != 'Manuell'

            # add default filter poles and zeroes to plot
            def def_points():
                clear_all_points()

                # save action and mode
                tmp = [self.action, self.mode]

                # update points
                # poles
                self.action = 'add'
                self.mode = 'p'
                for ind, pPoint in enumerate(cases[self.filter][0]):
                    order = cases[self.filter][2][ind]
                    self.update_point(pPoint, 'p', order)

                # zeroes
                self.action = 'add'
                self.mode = 'n'
                for ind, zPoint in enumerate(cases[self.filter][1]):
                    order = cases[self.filter][3][ind]
                    self.update_point(zPoint, 'z', order)

                # restore action and mode
                self.action = tmp[0]
                self.mode = tmp[1]

                # causal system
                # for future updates change arg to any no. greater than
                # max. real part of poles
                self.update_roc(1.1)

            # For sin and cos only:
            Omega1 = np.pi/4

            # default filters (usage: [ poles, zeroes, order_poles, order_zeroes ] )
            cases = {
                'man': [[], [], [], []],
                'unit': [[1], [0], [1], [1]],
                'sin': [[np.cos(Omega1) + 1j*np.sin(Omega1)], [0], [1], [1]],
                'cos': [[np.cos(Omega1) + 1j*np.sin(Omega1)], [0, np.cos(Omega1)], [1], [1, 1]]
            }

            def_points()
            update_widgets(filtr)

    def update_plot(self):
        # calculate transformations and plot in axh and axH
        poles = np.array(list((x.p for x in self.pp)), dtype=complex)
        poles_order = np.array(list((x.order for x in self.pp)))
        zeroes = np.array(list((x.p for x in self.pz)), dtype=complex)
        zeroes_order = np.array(list((x.order for x in self.pz)))
        roc = np.around(self.roc['sigma'], 5)

        # update n-domain
        h_n = np.real(rwth_transforms.iz_hn(self.n, self.H0, poles, zeroes, poles_order, zeroes_order, roc))
        rwth_plots.stem_set_ydata(self.handles['containerh'], np.real(h_n))
        if not np.isnan(h_n[0]):
            rwth_plots.update_ylim(self.handles['axh'], h_n, 0.19, ymax=1e5)
            self.pbzErrortxt.set_visible(False)
        else:
            self.pbzErrortxt.set_visible(True)
            rwth_plots.update_ylim(self.handles['axh'], np.zeros(self.n.shape), 0.19, ymax=1e5)

        self.systemIsStable = np.min(roc) < 1 < np.max(roc)

        # update f-domain
        if self.systemIsStable:
            H_f = np.abs(rwth_transforms.iz_Hf(
                self.f, self.H0, poles, zeroes, poles_order, zeroes_order, True
            )).round(8)
            rwth_plots.update_ylim(self.handles['axH'], H_f, 1.9, ymax=1e5)
        else:
            H_f = np.empty(self.f.shape)
            H_f[:] = np.nan
            rwth_plots.update_ylim(self.handles['axH'], [-0.5, 0.5], 0.1)

        self.stabilitytxt.set_visible(not self.systemIsStable)

        self.handles['lineH'].set_ydata(H_f)
