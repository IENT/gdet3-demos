{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": false,
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# Copyright 2020-2024 Institut für Nachrichtentechnik, RWTH Aachen University\n",
    "%matplotlib widget\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import interact, interactive, fixed, Layout\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import sys\n",
    "\n",
    "import rwth_nb.plots.mpl_decorations as rwth_plots\n",
    "from rwth_nb.misc.signals import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div>\n",
    "    <img src=\"figures/rwth_ient_logo@2x.png\" style=\"float: right;height: 5em;\">\n",
    "</div>\n",
    "\n",
    "# Fourier-Reihenkoeffizienten\n",
    "\n",
    "Zum Starten: Im Menü: Run <span class=\"fa-chevron-right fa\"></span> Run All Cells auswählen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Einleitung\n",
    "Im Folgenden soll die periodische Rechteckfunktion\n",
    "\n",
    "$$s_\\mathrm{p}(t) = \n",
    "\\mathrm{rect}\\left(\\frac{t}{T_1}\\right) \\ast\n",
    "\\sum_{n=-\\infty}^\\infty \\delta(t-nT)\n",
    "$$\n",
    "\n",
    "durch eine *endliche* Fourier-Summe $s_{\\mathrm{p},N}(t)$ approximiert werden."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = np.linspace(-3,3,10000) # t-axis\n",
    "T = 1; F = 1/T; # periodicity T\n",
    "T1 = T/2\n",
    "\n",
    "# Parts of the infinite rect sum\n",
    "Nmax = 3 # should be infinity :-)\n",
    "s = np.zeros_like(t)\n",
    "for n in np.arange(-Nmax,Nmax+1):\n",
    "    s = s + rect((t-n)/T1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# Plot\n",
    "fig,ax = plt.subplots(figsize=(6,3)); ax.plot(t, s, 'rwth:blue'); \n",
    "ax.set_xlabel(r'$\\rightarrow t$'); ax.set_ylabel(r'$\\uparrow s_{\\mathrm{p}}(t)$');\n",
    "ax.set_xlim([-2.9, 2.9]); ax.set_ylim([0, 1.09]); rwth_plots.axis(ax);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die zugehörigen Fourier-Reihenkoeffizienten lassen sich (wie im Buch, Formel (3.27)) berechnen zu\n",
    "\n",
    "$$\n",
    "S_\\mathrm{p}(k)\n",
    "=\n",
    "\\frac{\\sin\\left(k\\pi\\frac{T_1}{T}\\right)}{k\\pi}\\quad\\text{mit }k\\neq 0\n",
    "$$\n",
    "und \n",
    "$$\n",
    "S_\\mathrm{p}(k=0) = \\frac{T_1}{T}\\text{.}\n",
    "$$\n",
    " \n",
    "Im Folgenden ist $T=\\frac{1}{F} = 1$.\n",
    "\n",
    "Im Zeitbereich kann die Fourier-Summe durch eine endliche Summe (Summation bis zu $\\pm N$ anstelle $\\pm \\infty$) wie folgt angenähert werden: \n",
    "\n",
    "\n",
    "$$\n",
    "s_{\\mathrm{p},N}(t)\n",
    "=\n",
    "\\sum_{k=-N}^N S_\\mathrm{p}(k) \\mathrm{e}^{\\mathrm{j} 2 \\pi k F t}\n",
    "\\text{ .}\n",
    "$$\n",
    "\n",
    "Da $s_\\mathrm{p}(t)$ hier ein reelles Signal ist (und damit $S_\\mathrm{p}(-k) = S_\\mathrm{p}^\\ast(k)$ gilt), kann es somit über den Gleichanteil und die Fourier-Koeffizienten durch\n",
    "\n",
    "$$\n",
    "s_{\\mathrm{p},N}(t)\n",
    "=\n",
    "S_\\mathrm{p}(0) + 2 \\sum_{k=1}^N \\mathrm{Re}\\left\\{ S_\\mathrm{p}(k) \\mathrm{e}^{\\mathrm{j} 2 \\pi k F t} \\right\\}\n",
    "$$\n",
    "beschrieben werden.\n",
    "\n",
    "Zunächst für ein einfaches Beispiel mit $N=4$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 4\n",
    "k  = np.arange(0,N)\n",
    "# Fourier series coefficients\n",
    "Sp = np.zeros(N)\n",
    "Sp[0] = T1/T # k=0\n",
    "Sp[1::] = np.sin(k[1::]*np.pi*T1/T)/(k[1::]*np.pi) # k > 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# Plot\n",
    "fig,ax = plt.subplots(); rwth_plots.stem(ax, k, Sp, 'rwth:blue');\n",
    "ax.set_xlabel(r'$\\rightarrow k$'); ax.set_ylabel(r'$\\uparrow S_{\\mathrm{P}}(k)$'); rwth_plots.axis(ax);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Abgebildet sind die Koeffizienten $S_p(k)$ für $k=0,...,3$. Aus diesen vier Koeffizienten wird nun das Ursprungssignal angenähert. \n",
    "\n",
    "Die folgende Abbildung zeigt die einzelnen Summanden \n",
    "$\n",
    "2 \\mathrm{Re}\\left\\{ S_\\mathrm{p}(k) \\mathrm{e}^{\\mathrm{j} 2 \\pi k F t} \\right\\}\n",
    "$\n",
    "sowie das Resultat $s_{\\mathrm{p},N}(t)$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Approximated time signal\n",
    "sp = Sp[0]*np.ones_like(t) # DC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "fig,axs = plt.subplots(2,1); colors = ['rwth:green', 'rwth:magenta', 'rwth:orange']; ax = axs[0];\n",
    "ax.plot(t,sp, 'rwth:blue-50', linestyle='--', label=r'$S_\\mathrm{p}(0)$')\n",
    "legend = np.array([r'$k==0$'])\n",
    "for kIter in k[1::]:\n",
    "    spIter = 2*np.real(Sp[kIter]*np.exp(2j*np.pi*kIter*F*t));\n",
    "    sp = sp + spIter;\n",
    "    # Plot\n",
    "    ax.plot(t,spIter, color=colors[kIter-1], linestyle='--', label=r'$k={}$'.format(kIter))\n",
    "    \n",
    "ax.set_xlabel(r'$\\rightarrow t$'); ax.legend(loc='lower left');\n",
    "ax.set_xlim([-1.6,1.6]); ax.set_ylim([-1.1,1.1]); rwth_plots.axis(ax); \n",
    "\n",
    "ax = axs[1]; ax.plot(t, sp)\n",
    "ax.set_xlabel(r'$\\rightarrow t$'); ax.set_ylabel(r'$\\uparrow s_{\\mathrm{P},N}(t)$');\n",
    "ax.set_xlim([-1.6,1.6]); rwth_plots.axis(ax);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die verwendeten Koeffizienten erzeugen Signalanteile mit ansteigender Frequenz. \n",
    "Obwohl für die Rekonstruktion nur vier Koeffizienten verwendet wurden, ist die Annäherung an das ursprüngliche Rechtecksignal zu erkennen. Für eine klarere Darstellung der Ecken im Signal fehlen die hochfrequenten Anteil der höheren Koeffizienten. In der folgenden Demonstration kann dieser Effekt anschaulich betrachtet werden. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Demo\n",
    "\n",
    "Die Demonstration ermöglicht die Untersuchung des Einflusses der Anzahl der zur Rekonstruktion verwendeten Koeffizienten $N$ und der Breite $T_1$ des verwendeten Rechtecks. \n",
    "Über den Slider können verschiedene Werte für $N$ und $T_1$ eingestellt werden. Die gestrichelte schwarze Linie in der oberen Abbildung zeigt das zu approximierende Ausgangssignal, die blaue Linie die errechnete Annäherung unter Verwendung von $N$ Koeffizienten. Die verwendeten Koeffizienten sind in der unteren Abbildung dargestellt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": false,
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "fig,axs = plt.subplots(2,1);\n",
    "N_wdgt = widgets.IntSlider(min=1, max=50, step=1, description='$N$', continuous_update=False)\n",
    "T1_wdgt = widgets.FloatSlider(min=0.125, max=.75, step=0.125, value=.5, description='$T_1$', continuous_update=False, readout_format='.3f')\n",
    "@widgets.interact(N=N_wdgt, T1=T1_wdgt)\n",
    "def update_plot(N, T1):\n",
    "    # Snippet of time signal\n",
    "    s = np.zeros_like(t)\n",
    "    for n in np.arange(-Nmax,Nmax+1):\n",
    "        s = s + rect((t-n)/T1)\n",
    "    \n",
    "    # Fourier series coefficients\n",
    "    k  = np.arange(0,N)\n",
    "    Sp = np.zeros(N)\n",
    "    Sp[0] = T1/T\n",
    "    Sp[1::] = np.sin(k[1::]*np.pi*T1/T)/(k[1::]*np.pi)\n",
    "    \n",
    "    # Approximated time signal\n",
    "    sp = Sp[0]*np.ones_like(t) # DC\n",
    "    for kIter in k[1::]:\n",
    "        sp = sp + 2*np.real(Sp[kIter]*np.exp(2j*np.pi*kIter*F*t));\n",
    "\n",
    "    k = np.concatenate((-k[::-1],k[1::]));\n",
    "    Sp = np.concatenate((Sp[::-1],Sp[1::]))\n",
    "\n",
    "    # Plot\n",
    "    if not axs[0].lines: # create plot\n",
    "        ax = axs[0]; ax.plot(t, s, 'k--')\n",
    "        ax.plot(t, sp, 'rwth:blue'); \n",
    "        ax.set_xlabel(r'$\\rightarrow t$'); ax.set_ylabel(r'$\\uparrow s_{\\mathrm{p},N}(t)$');\n",
    "        ax.set_xlim([-1.6,1.6]); ax.set_ylim([-.2, 1.2]); rwth_plots.axis(ax);\n",
    "        \n",
    "        ax = axs[1]; rwth_plots.stem(ax, k, Sp, 'rwth:blue');\n",
    "        ax.set_xlabel(r'$\\rightarrow k$'); ax.set_ylabel(r'$\\uparrow S_{\\mathrm{p}}(k)$');\n",
    "        rwth_plots.axis(ax); ax.set_ylim([-.29,.89])\n",
    "    else: # update plot\n",
    "        axs[0].lines[0].set_ydata(s); axs[0].lines[1].set_ydata(sp);\n",
    "        rwth_plots.stem_set_data(axs[1].containers[0], k, Sp);plt.show()\n",
    "        axs[1].set_xlim([-N+.5, N-.5])\n",
    "\n",
    "def on_press(event):\n",
    "    print('press', event.key)\n",
    "    sys.stdout.flush()\n",
    "    if event.key == 'left':\n",
    "        N_wdgt.value -= N_wdgt.step\n",
    "    elif event.key == 'right':\n",
    "        N_wdgt.value += N_wdgt.step\n",
    "    elif event.key == 'up':\n",
    "        T1_wdgt.value += T1_wdgt.step\n",
    "    elif event.key == 'down':\n",
    "        T1_wdgt.value -= T1_wdgt.step\n",
    "fig.canvas.mpl_connect('key_press_event', on_press);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Aufgaben\n",
    "\n",
    "* Variiere $N$ und halte $T_1=0.5$ konstant. Wieviele Koeffizienten erhält man für $N=10$ und wie sind diese angeordnet? Warum?\n",
    "\n",
    "* Ab welchem $N$ ist annähernd die Struktur des Ausgangssignals zu erkennen? \n",
    "\n",
    "* Setze $N=50$. Wie verhält sich das rekonstruierte Signal an den Ecken des Rechteckimpulses? Warum?\n",
    "\n",
    "* Variiere nun $T_1$. Setze $N=7$ und beobachte, was mit den Koeffizienten passiert, wenn $T_1$ kleine oder große Werte annimmt. Was passiert mit dem approximierten Signal?\n",
    "\n",
    "* Für welche Kombination der Werte $N$ und $T_1$ kann die angenäherte Stuktur des Ausgangssignal erkannt werden? Warum?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "This notebook is provided as [Open Educational Resource](https://en.wikipedia.org/wiki/Open_educational_resources) (OER). Feel free to use the notebook for your own purposes. The code is licensed under the [MIT license](https://opensource.org/licenses/MIT). \n",
    "\n",
    "Please attribute the work as follows: \n",
    "*Christian Rohlfing, Übungsbeispiele zur Vorlesung \"Grundgebiete der Elektrotechnik 3 - Signale und Systeme\"*, gehalten von Jens-Rainer Ohm, 2020, [Institut für Nachrichtentechnik](http://www.ient.rwth-aachen.de), RWTH Aachen University."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
